# Explorador de API de Rick & Morty
Versión alfa 1

El explorador de API de Rick y Morty es una aplicación web desarrollada fullstack en Javascript (Node.JS y React) que implementa la API pública de Rick y Morty y agrega ejemplos de usabilidad, navegación, consulta y gestión CRUD.

Esta aplicación se desarrolla con el objeto de dar solución a la evaluación técnica para la búsqueda de desarrolladores senior fullstack en Datatraffic.

Se utilizó una arquitecura Cliente / Servidor básica con un desarrollo basado en componentes CDD.

## INSTALACION
- Clonar el repositorio y ejecutar:

- ### FRONTEND
    - cd ./client
    - npm install

- ### BACKEND
    - cd ./server
    - npm install

## COMPILACIÓN FRONTEND
- Para compilar React (cuando haya sido modificado) se debe ejecutar npm run build en ./client

## EJECUCION
- ### FRONTEND
    - cd ./client
    - npm run start:dev

- ### BACKEND
    - cd ./server
    - npm run start:dev

## CAPTURAS
![](./_README/1.png)
![](./_README/2.png)
![](./_README/3.png)

## FEATURES
- Consume la API de RyM online a través del backend propio
- El backend expone API para manejo de CRUD de personajes
- La entidad personajes existe localmente en una base de datos SQLite
- Se incluyó CRUD en frontend para personajes
- Se agregó personalización por tema Bootswatch intercambiable en index.html
- [new] Se implemto un login para acceder a la aplicacion y un boton para registrar un usuario nuevo
- [new] Se implemento un boton de logout para cerrar sesion correctamente
- [new] Se implemento autenticacion por token jwt al momento rde realizar llamados al Api (los token duran 1 hora, despues de este tiempo es necesario cerrar sesion y volver a loguearse)
- [new] Creada la pagina de mapa de calor que nos muestra un mapa de calor dependiendo la cantidad de personajes que salen en un episodio del show.
- [fix] Se mejoro el crud del Api


## STACK UTILIZADO
- Node JS
- Webpack
- React JS
- React Router
- Bootstrap
- Bootswatch
- [new] Material
- SQLite
- Git y npm

## Consoderaciones para la prueba

### Analisis
A la hora de realizar el análisis de requisitos fue muy importante leer y comprender las instrucciones entregadas para realizar la prueba. A su vez fue necesario entender la aplicación base sobre la que se debería trabajar. Para esto se consultó un poco de la documentación de la api original y se realizaron pruebas manuales para examinar como funcionaba la aplicación base. Después de esto fue necesario definir como se iba a proceder y priorizar en lo que se debía trabajar. Para esto se identificaron y crearon los siguientes requerimientos funcionales:

#### BackEnd
##### Implementar el sistema de autenticación:
El usuario podrá y deberá autenticarse en el servidor para poder acceder a todas las rutas del mismo.
-In: token jwt
-Out: autorización o error al no presentar o presentar fallos al decodificar las credenciales (por ejemplo token vencido).

##### Crear Login
El usuario podrá acceder a su cuenta para obtener un token de autenticación que le permita utilizar los servicios.
-In: Credenciales de logro
-Out: Token jwt

##### Crear lógica para el HeatMap
El usuario podrá solicitar una matriz donde tendrá la aparición de cada personaje por episodio de la serie.
-In: Solicitud de la lógica para el heatmap
-Out: la lógica del heatmap, representada como una matriz en la que almacenamos los personajes que salen en cada episodio.

#### FrontEnd

##### Implementar sistema de Logeo:
El usuario podrá ingresar su usuario y contraseña para poder acceder a la aplicación.
-In: credenciales de autenticación
-Out: autorización o error al no presentar o presentar fallos al decodificar las credenciales (por ejemplo token vencido).

##### Implementar sistema de registro:
El usuario podrá ingresar su usuario y contraseña para poder crear una cuenta con la cual ingresar a la aplicación. Es necesario loguearse posterior a la creación de una cuenta.
-In: Credenciales de logro
-Out: Usuario creado o Error

##### Crear página para el HeatMap
El usuario podrá escoger en el menú que desea ver el heatmap, una vez seleccionada esta opción cargará el mapa de calor realizado.
-In: solicitud de la página del heatmap
-Out: Heatmap de personajes

#### Logout
El usuario puede terminar su sesión para salir de la aplicación.
-In: solicitud de logout
-Out: página de inicio

### Desarrollo
Una vez establecidos los requerimientos, se inició a trabajar inicialmente en el servidor de la aplicación. Se implementó la autenticación mediante tokens jwt (generación y verificación). Una vez realizado esto se implementó el registro y el login para esto se utilizó la base de datos local para crear una tabla que guarde las credenciales de los usuarios. Finalmente se realizó el endpoint solicitado para el heatmap junto a los servicios de Episodios.

Teniendo una versión del servidor funcional, para el cliente se inicio realizando la propagación del token obtenido previamente, posteriormente se implemente el logout para eliminar el token con lo cual se queda invalidada la sesión actual. Después se creó la página del heatmap, donde se utilizó una libreria externa para realizar y mostrar el mapa de calor. Finalmente se creó la página de login pasando por varias versiones y funciones, y en esta página se decidió incluir el botón de registro para poder crear un usuario para utilizar la aplicación.

### Complicaciones
A la hora de realizar el análisis fue difícil entender cómo funcionaba la aplicación pues ésta posee una gran cantidad de tecnologías y estaba un poco desactualizada. Para poder implementar los requerimientos, se realizaron investigaciones en la documentación para decidir cómo implementar dichos requerimientos.


## AUTOR
- Julio J. Yépez (@jjyepez)
- Juan Andres Salazar
