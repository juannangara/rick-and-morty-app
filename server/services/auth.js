require('dotenv').config();

const jwt = require('jsonwebtoken');
const path   = require('path');
const sqlite = require('sqlite3');
const {open} = require('sqlite');

const secretKey = process.env.SECRET_KEY;


const generarToken = (username) => {
  return jwt.sign({ username },  secretKey, { expiresIn: '1h' });
};

const verificarToken = (token, callback) => {
  jwt.verify(token,  process.env.SECRET_KEY, callback);
};

async function createUser(user){
  let dbname = process.env.CRUD_DBNAME || 'data';
  let db_entity = 'users';

  // abrir / crear la BD
  let db = await open({
      filename: path.resolve(`./data/${dbname}.db`),
      driver  : sqlite.Database
  })
  .catch(err => {
      console.log({err});
      res
          .status(500)
          .json({
              error: true,
              ...err
          });
  });

  // consulta para obtener el modelo
  let columns = Object.keys(user);

  // crear la entidad
  let entityCols = columns.map(key => {
      let type = key === 'id' ? 'INTEGER PRIMARY KEY AUTOINCREMENT' : 'TEXT';
      return `${key} ${type}`
  })
  .join(',');
  
  let sql = `
      CREATE TABLE IF NOT EXISTS ${db_entity} (${entityCols});
  `;
  let table = await db.exec(sql)
  .catch(err => {
      console.log({err});
      throw new Error('Error al acceder a la base de datos');
  });

  
  
  const value = Object.values(user).map(
    valor => 
      typeof valor === 'string' ? `'${valor}'` : valor
    ).join(', ');

  columns = Object.keys(user).join(', ');
  

  sql = ` INSERT INTO ${db_entity} (${columns}) VALUES (${value});`;
 

  const result = await db.run(sql)
  .catch(err => {
      console.log({err});
      throw new Error('Error al insertar usuario');
  });

  console.log("insertado")

  db.close();
}

async function login(user){
  const {
      username = '',
      password = ''
  } = user;

  let dbname = process.env.CRUD_DBNAME || 'data';
  let db_entity = 'users';
 
  const knex = require('knex')({
      client: 'sqlite3',
      useNullAsDefault: true,
      connection: {
          filename: path.resolve(`./data/${dbname}.db`),
      },
  });

  const sql = `
  SELECT *
  FROM users
  WHERE username = ?
  AND password = ?;
`;

try {
  const usuario = await knex.raw(sql, [user.username, user.password]);
 
  if (usuario && usuario.length > 0) {
    console.log('Usuario encontrado:', usuario[0].username);
  } else {
    return null;
  }


  knex.destroy();
  return generarToken(user.username);

} catch (error) {
  console.error('Error al ejecutar la consulta:', error);
  throw  new Error('Error al buscar el usuario');
}
 
}

module.exports = { generarToken, verificarToken, createUser, login};
