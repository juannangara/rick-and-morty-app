const fetch = require('node-fetch');

require('dotenv').config();

const apiBaseURL = process.env.API_BASE_URL || 'http://localhost' 

module.exports = {
    getEpisodesAll,
    getEpisodeById,
    getEpisodesHeatmap,
}

async function getEpisodesAll(options){
    const {
        page = 1
    } = options;

    let url = `${apiBaseURL}/episode/?page=${page}`;
    let rspJson = await fetch(url)
    .then(rslt => rslt.json())
    .catch(err => {
        console.log({err});
    });
    return rspJson;
}

async function getEpisodeById(id){
    let url = `${apiBaseURL}/episode/${id}`;
    let rspJson = await fetch(url)
    .then(rslt => rslt.json())
    .catch(err => {
        console.log({err});
    });
    return rspJson;
}

async function getEpisodesHeatmap(){
    let episodes = [];
    let nextUrl = `${apiBaseURL}/episode/`;
   
    do{
        
    const rspJson = await fetch(nextUrl);
    const episodeResponse = await rspJson.json();
    nextUrl = episodeResponse.info.next;
    console.log(episodeResponse.info)
    episodes.push(...episodeResponse.results.map((episode) => ({
        episodeName: episode.name,
        season: Number(episode.episode.split("E")[0].replaceAll("S", "")),
        episodeNumber: Number(episode.episode.split("E")[1]),
        numberOfCharacters: episode.characters.length,
    })));

    }while(nextUrl)
    
    return episodes;   
}