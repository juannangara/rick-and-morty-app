let router = require('express').Router();

const service = require('../services/auth');

router
    .post('/login', async (req, res, next) => {
        const {
            username,
            password
        } = req.body;

        logUser = {username,password}

        try {
            const usuario = await service.login(logUser);
            if (usuario) {
              res.status(200).json(usuario);
            } else {
              res.status(401).json({ error: 'Credenciales inválidas' });
            }
          } catch (error) {
            console.error('Error al procesar la solicitud:', error);
            res.status(500).json({ error: 'Error al procesar la solicitud' });
          }
       
    })

    .post('/createUser', async (req, res, next) => {
        const {
            username,
            password
        } = req.body;
       
        newUser = {username,password}

        try{
        const usuario = await service.createUser(newUser);
            
        res
        .status(200)
        .json("usuario creado");
        }catch{
        res
        .status(500)
        .json("error al crear un usuario");
        }
      
    })
;

module.exports = router;