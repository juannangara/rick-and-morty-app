let router = require('express').Router();
const tokenservice = require('../services/auth');


router
    .all('*', (req, res, next) => {
        if (req.path.startsWith('/auth'))
        {
            console.log("logeando")
            next();
        }else {
            
            const token = req.header('Authorization');
            if (!token) {
              return res.status(401).json({ mensaje: 'Token no proporcionado' });
            }
            try {
              const decoded = tokenservice.verificarToken(token, "");
            } catch (error) {
              console.error('Error al verificar el token:', error);
              return res.status(401).json({ mensaje: 'Token no válido' });
            }      
           
           console.log("autorizado");
           next();
        }
    })
    
    .use('/characters', require('./characters'))
    .use('/locations', require('./locations'))
    .use('/episodes', require('./episodes'))
    .use('/auth', require('./login'))
    .use('/crud', require('./crud'))
;

module.exports = router;