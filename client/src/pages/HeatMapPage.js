import React, { useContext, useEffect, useState } from 'react';

import ContentPage from '../components/ContentPage';
import getAuthHeaders from "../utils/getAuthHeaders";

import { HeatMapGrid } from 'react-grid-heatmap';
import { UserAuthContext } from '../components/Layout';

function HeatMapPage() {
    const { makeUserInvalid } = useContext(UserAuthContext);
    const [data, setData] = useState([]);
    const [heatmapData, setHeatmapData] = useState();
    const [seasonsLabels, setSeasonsLabels] = useState([]);
    const [episodesLabels, setEpisodesLabels] = useState([]);

    useEffect(async () => {
        loadEpisodes();
        let currentPage = document.location.search;
    }, []);

    async function loadEpisodes(page=1){
        setData([]);

        let url = `http://localhost:7000/episodes/heatmap`;
        
        let jsonRslt = await fetch(url, {
                headers: {
                    ...getAuthHeaders(),
                }
            })
            .then(rslt => rslt.json())
            .catch(err => {console.log({err}); makeUserInvalid();})
        ;
        const heatmap = [[]]
        jsonRslt.forEach((episode) => {
            if(!heatmap[episode.season - 1]) {
                heatmap[episode.season - 1] = [];
            }
            heatmap[episode.season - 1][episode.episodeNumber - 1] = episode.numberOfCharacters
        });
        setHeatmapData(heatmap);
        setSeasonsLabels([ ...Array(heatmap.length).fill(null).map((_, idx) => idx + 1), "s/e"]);
        const maxEpisodes = heatmap.reduce((prev, season) => season.length > prev ? season.length : prev, -1) ;
        setEpisodesLabels(Array(maxEpisodes).fill(null).map((_, idx) => idx + 1))
    }

    return (
        <ContentPage>
            <legend className='pb-2'>Mapa de calor de Personajes por Temporada y Episodio.</legend>
            {!!heatmapData ?
            <HeatMapGrid
                data={heatmapData}
                xLabels={episodesLabels}
                yLabels={seasonsLabels}
                cellHeight='2rem'
                cellRender={(x, y, value) => (
                    <div title={`Pos(${x}, ${y}) = ${value}`}>{value}</div>
                  )}
                cellStyle={(_x, _y, ratio) => ({
                    background: `rgb(12, 160, 44, ${ratio})`,
                    fontSize: '.8rem',
                    color: `rgb(0, 0, 0, ${ratio / 2 + 0.4})`
                })}
                square
                xLabelsPos='bottom'
    />:null}
        </ContentPage>
    )
}

export default HeatMapPage;