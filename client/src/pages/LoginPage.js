import React, { useState } from 'react';
import Alert from '@mui/material/Alert';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import Paper from '@mui/material/Paper';
import Snackbar from '@mui/material/Snackbar';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { green } from '@mui/material/colors';

const theme = createTheme({
  palette: {
    primary:{
      main: '#e95420',
    },
    secondary: green,
  },
});

function LoginPage() {

  const [isUserRegistered, setIsUserRegistered] = useState(true);



    const handleLogin = async (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);

    const formValues = {
      username: data.get('usuario'),
      password: data.get('password'),
    };

    try {
      const url = 'http://localhost:7000/auth/login';
      const response = await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          username: formValues.username,
          password: formValues.password,
        }),
      });
      const token = await response.json();

      if (!response.ok){
        throw new Error(`Error en la solicitud: ${response.status}`);
      }

      localStorage.setItem('user', token);
      location.reload();
      
    } catch(error) {
      alert("Ha ocurrido un error, comprueba el usuario y la contraseña");
      console.log(error);
    };  
    }

    const handleRegister = async (event) => {
      event.preventDefault();
      const data = new FormData(event.currentTarget);
  
      const formValues = {
        username: data.get('usuario'),
        password: data.get('password'),
      };
  
      try {
        const url = 'http://localhost:7000/auth/createUser';
        const response = await fetch(url, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            username: formValues.username,
            password: formValues.password,
          }),
        });
        const token = await response.json();
  
        if (!response.ok){
          throw new Error(`Error en la solicitud: ${response.status}`);
        }
  
        alert("Usuario creado correctamente, por favor inicie sesion");
        setIsUserRegistered(true);
        
      } catch(error) {
        alert("Ha ocurrido un error al crear el usuario");
        console.log(error);
      };  
      }

    return (
      <ThemeProvider theme={theme}>
      <Grid container className="d-flex justify-content-center mt-5">
        <Grid item xs={10} sm={6} md={5} lg={3}>
          <Paper sx={{ p: 2, margin: 'auto', flexGrow: 1 }}>
            <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
              <img alt="Rick&&MortyApp icon" src="../../static/img/rym_logo-50pcnt.svg" width="50%" />
              <Typography component="h1" variant="h5">
                Inicio de sesión
              </Typography>
              <form onSubmit={isUserRegistered? handleLogin:handleRegister}>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="usuario"
                  label="Usuario"
                  name="usuario"
                  autoFocus
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label="Contraseña"
                  type="password"
                  id="password"
                />
                <Button
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2 }}
                  type="submit"
                  color="primary"
                >
                  Iniciar sesión
                </Button>
                <Button
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2 }}
                  type="submit"
                  color="primary"
                  onClick={(e) => setIsUserRegistered(false)}
                >
                  Registrarse
                </Button>
              </form>
            </Box>
          </Paper>
        </Grid>
      </Grid>
      </ThemeProvider>
    )
}

export default LoginPage;