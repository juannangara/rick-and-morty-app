export default function getAuthHeaders() {
  const token = localStorage.getItem('user');
  return {
      Authorization: token
  }
}