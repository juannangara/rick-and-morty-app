import React, { useEffect, useState } from 'react';
import {MemoryRouter, Redirect, Route, Switch} from 'react-router-dom';
import CacheRoute, {CacheSwitch} from 'react-router-cache-route';

import MenuBar from './MenuBar';
import Content from './Content';
import SideBar from './SideBar';
import ContentBody from './ContentBody';

import CharactersPage from '../pages/CharactersPage';
import LocationsPage from '../pages/LocationsPage';
import EpisodesPage from '../pages/EpisodesPage';
import HeatmapPage from '../pages/HeatMapPage';
import LoginPage from '../pages/LoginPage';

import './css/Layout.css';

export const UserAuthContext = React.createContext(null);

function Layout(props){
    const [isUserValid, setIsUserValid] = useState(true);
    useEffect(() => {
        const token = localStorage.getItem('user');
        if(!token) {
            setIsUserValid(false);
        }
    }, []);
    return (
        <div className='Layout'>
            <MenuBar title='Exploador de API de Rick & Morty'/>
            <UserAuthContext.Provider value={{
                makeUserInvalid: () => { localStorage.removeItem('user'); setIsUserValid(false); }
            }}>
            {!isUserValid ? <LoginPage/>: <MemoryRouter>
                <Content>
                    <SideBar />
                    <ContentBody>
                        <CacheSwitch>
                            <CacheRoute path='/characters'>
                                <CharactersPage />
                            </CacheRoute>
                            <CacheRoute path='/characters/p/:page'>
                                <CharactersPage />
                            </CacheRoute>
                            <CacheRoute path='/locations'>
                                <LocationsPage />
                            </CacheRoute>
                            <CacheRoute path='/episodes'>
                                <EpisodesPage />
                            </CacheRoute>
                            <CacheRoute path='/heatmap'>
                                <HeatmapPage />
                            </CacheRoute>
                           
                            <Redirect to='/characters' />
                        </CacheSwitch>
                    </ContentBody>
                </Content>
            </MemoryRouter>}
            </UserAuthContext.Provider>
        </div>
    )
}

export default Layout;