import React, { useContext } from 'react';
import {Link} from 'react-router-dom';

import {MapPin, Users, Video, LogOut, Thermometer } from 'react-feather';

import './css/SideBar.css';
import { UserAuthContext } from './Layout';

function SideBar() {
    const { makeUserInvalid } = useContext(UserAuthContext);
    // TODO, seleccionar menú actual
    return (
        <div className='SideBar p-0 pt-4'>
            <ul className="nav flex-column">
                <li className="nav-item">
                    <Link className="nav-link" to="characters">
                        <Users /><span className='pl-3'>Personajes</span>
                    </Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="locations">
                        <MapPin /><span className='pl-3'>Lugares</span>
                    </Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="episodes">
                        <Video /><span className='pl-3'>Episodios</span>
                    </Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="heatmap">
                        <Thermometer /><span className='pl-3'>Mapa de calor</span>
                    </Link>
                </li>
                <li className="nav-item">
                    <button className="nav-link" style={{ border: 0, background: "none", color: "#e95420"}} onClick={() => { makeUserInvalid(); location.reload() }}>
                        <LogOut /><span className='pl-3'>Cerrar Sesión</span>
                    </button>
                </li>
            </ul>
        </div>
    )
}

export default SideBar;